gantt.locale = {
	date: {
		month_full: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤษจิกายน", "ธันวาคม"],
		month_short: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ษ.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พฤ.ค.", "ธ.ค."],
		day_full: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"],
		day_short: ['อ.', "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."]
	},
	labels: {
		add_template_btn: "เพิ่ม ประเภทงาน",
		add_holder_btn: "เพิ่ม ผู้รับผิดชอบ",
		new_task: "เพิ่มงานใหม่",
		icon_save: "บันทึก",
		icon_cancel: "ยกเลิก",
		icon_details: "รายละเอียด",
		icon_edit: "แก้ไข",
		icon_delete: "ลบ",
		confirm_closing: "", //Your changes will be lost, are your sure ?
		confirm_deleting: "ข้อมูลจะถูกลบอย่างถาวร แน่ใจหรือไม่ที่จะทำ ?",
		section_description: "รายละเอียด",
		section_time: "ช่วงเวลา",
		section_type: "Type",
		section_template: "ประเภท",
		section_holder: 'ผู้รับผิดชอบ',
		section_task_name: 'ทะเบียนรถ หรือ ชื่องาน',
		section_start_date: "วันที่เริ่ม",
		section_duration: "ระยะเวลา",

		/* grid columns */

		column_text: "ชื่องาน",
		column_start_date: "เวลาเริ่ม",
		column_duration: "ระยะเวลา",
		column_add: "",

		/* link confirmation */
		link: "Link",
		confirm_link_deleting: "กำลังจะลบ",
		link_start: " (เริ่ม)",
		link_end: " (สิ้นสุด)",

		type_task: "งาน",
		type_project: "โปรเจ็กต์",
		type_milestone: "Milestone",

		minutes: "นาที",
		hours: "ชั่วโมง",
		days: "วัน",
		weeks: "สัปดาห์",
		months: "เดือน",
		years: "ปี",

		/* message popup */
		message_ok: "ตกลง",
		message_cancel: "ยกเลิก",
		template: "เท็มเพลท"
	}
};